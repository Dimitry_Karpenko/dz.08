<?php
include_once 'classes/Entries.php';
include_once 'classes/Comments.php';
include_once 'config/db.php';

$entries = Entries::all($pdo);
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <style>
    .col{
      margin: 5px;
      padding: 10px;
      background: #91bce6;
    }
    li, .blog{
      display: flex;
      flex-direction: row;
    }

  </style>
</head>
<body>

  <div>
    <a href="createTable.php">Создать таблицу</a>
  </div>

  <h1>Блог</h1>
  <div>
    <a href="entries/addEntry.php">Создать новую запись блога</a>
  </div>
  <div>
    <?php if (!empty($entries)):?>
    <?php foreach ($entries as $entry):?>
      <div class="col">
        <h2>
          <a href="entries/viewEntry.php?id=<?=$entry->getId();?>"><?=$entry->getTitle();?></a>
        </h2>

        <div class="blog">

          <form action="entries/deleteEntry.php" method="post">
            <input type="hidden" name="id" value="<?=$entry->getId();?>">
            <input type="submit" name="submit" value="Удалить блог">
          </form>

          <form action="entries/editEntry.php" method="post">
            <input type="hidden" name="id" value="<?=$entry->getId();?>">
            <input type="submit" name="submit" value="Редактировать блог">
          </form>

        </div>

        <p>
          <?=$entry->getIntro();?> (<?=$entry->getContent();?>)
        </p>
        <ul>
          <h4>Комментарии к теме блога</h4>
          <?php foreach ($entry->getComments($pdo) as $comment):?>
            <li>
              <?=$comment->getBody();?> (автор:<?=$comment->getName();?>)
              <form action="comments/editComment.php" method="post">
                <input type="hidden" name="id" value="<?=$comment->getId();?>">
                <input type="submit" name="submit" value="редактировать">
              </form>
              <form action="comments/deleteComment.php" method="post">
                <input type="hidden" name="id" value="<?=$comment->getId();?>">
                <input type="submit" name="submit" value="удалить">
              </form>
            </li>
          <?php endforeach;?>
        </ul>




        <form action="comments/storeComment.php" method="post">
          <input type="hidden" name="entry_id" value="<?=$entry->getId();?>">
          <div>
            <label for="body">Комментарий</label>
            <input type="text" name="body" id="body">
            <label for="name">Имя</label>
            <input type="name" name="name" id="name">
            <input type="submit" name="submit" value="отправить">
          </div>

        </form>
      </div>
    <?php endforeach;?>
    <?php endif;?>
  </div>


</body>
</html>
