<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
<h1>Нова запись блога</h1>

<div>
  <form action="storeEntry.php" method="POST">

    <div>
      <label for="title">Заголовок</label>
      <input type="text" name="title" id="title">
    </div>

    <div>
      <label for="intro">Введение</label>
      <input type="text" name="intro" id="intro">
    </div>

    <div>
      <label for="content">Основной текст</label>
      <input type="text" name="content" id="content">
    </div>

    <div>
      <input type="submit" name="submit" value="Сохранить">
    </div>

  </form>
</div>

</body>
</html>
