<?php

require_once '../config/db.php';
require_once '../classes/Entries.php';

if (isset($_POST['submit']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }

  $_POST['id'] = intval($_POST['id']);

  Entries::edit($_POST['id'], $_POST['title'], $_POST['intro'], $_POST['content'], $pdo);

  header('location:../index.php');

}
