<?php

require_once '../config/db.php';
require_once '../classes/Entries.php';

if (isset($_REQUEST['id']))
{
  foreach ($_REQUEST as $key => $value)
  {
    $_REQUEST[$key] = htmlspecialchars($value);
  }
  $entry = Entries::getById($_REQUEST['id'], $pdo);
}
?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>

  <h1>
    <?=$entry->getTitle()?>
  </h1>
  <h2>
    <?=$entry->getIntro()?>
  </h2>
  <h3>
    <?=$entry->getContent()?>
  </h3>

</body>
</html>


