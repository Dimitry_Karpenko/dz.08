<?php

require_once '../config/db.php';
require_once '../classes/Entries.php';

if (isset($_REQUEST['submit']))
{
  foreach ($_REQUEST as $key => $value)
  {
    $_REQUEST[$key] = htmlspecialchars($value);
  }

  $entry = Entries::getById($_REQUEST['id'], $pdo);
}

?>

<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
<h1>Нова запись блога</h1>

<div>
  <form action="updateEntry.php" method="POST">

    <div>
      <input type="hidden" name="id" id="id" value="<?=$entry->getId()?>">
    </div>

    <div>
      <label for="title">Заголовок</label>
      <input type="text" name="title" id="title" value="<?=$entry->getTitle()?>">
    </div>

    <div>
      <label for="intro">Введение</label>
      <input type="text" name="intro" id="intro" value="<?=$entry->getIntro()?>">
    </div>

    <div>
      <label for="content">Основной текст</label>
      <input type="text" name="content" id="content" value="<?=$entry->getContent()?>">
    </div>

    <div>
      <input type="submit" name="submit" value="Сохранить">
    </div>

  </form>
</div>

</body>
</html>
