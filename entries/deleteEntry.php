<?php

require_once '../config/db.php';
require_once '../classes/Entries.php';

if (isset($_REQUEST['id']))
{
  foreach ($_REQUEST as $key => $value)
  {
    $_REQUEST[$key] = htmlspecialchars($value);
  }
  Entries::delete($_REQUEST['id'], $pdo);
}
