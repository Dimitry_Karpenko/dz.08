<?php
require_once '../config/db.php';
require_once '../classes/Entries.php';

if (isset($_POST['submit']))
{
  foreach ($_REQUEST as $key => $value)
  {
    $_REQUEST[$key] = htmlspecialchars($value);
  }

  $entry = new Entries($_REQUEST['title'], $_REQUEST['intro'], $_REQUEST['content']);

  $entry->store($pdo);

}
