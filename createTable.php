<?php

require_once 'config/db.php';


try
{
  $entrySql = '
          CREATE TABLE entries
          (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            title VARCHAR (255),
            intro VARCHAR (255),
            content VARCHAR (255)
          )
          DEFAULT CHARACTER SET utf8 ENGINE=InnoDB
         ';
  $pdo->exec($entrySql);

  $commentSql = 'CREATE TABLE comments
                 (
                    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    name VARCHAR (255),
                    body VARCHAR (255),
                    entry_id INT,
                    FOREIGN KEY (entry_id) REFERENCES entries (id)
                 )
                 DEFAULT CHARACTER SET utf8 ENGINE=InnoDB
                ';
  $pdo->exec($commentSql);

  header('location: index.php');
}
catch (Exception $exception)
{
  echo 'Не удалось создать таблицу в БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
}
