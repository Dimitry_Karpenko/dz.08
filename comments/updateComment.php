<?php
require_once '../config/db.php';
require_once '../classes/Comments.php';

if (isset($_POST['submit']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }
//  var_dump($_POST);
  Comments::edit($_POST['id'], $_POST['name'], $_POST['body'], $_POST['entry_id'], $pdo);

  header('location: ../index.php');
}
