<?php

require_once '../classes/Comments.php';
require_once '../config/db.php';

if (isset($_POST['submit']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }

  $commentObj = new Comments($_POST['name'], $_POST['body'], $_POST['entry_id']);
  $commentObj->store($pdo);

}
