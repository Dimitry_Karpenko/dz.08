<?php

require_once '../config/db.php';
require_once '../classes/Comments.php';
require_once '../classes/Entries.php';

if (isset($_POST['submit']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }

  $comment = Comments::getById($_POST['id'], $pdo);
}
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>
  <h1>Редактировать комментарий</h1>
  <div>
    <form action="updateComment.php" method="post">

      <input type="hidden" name="id" value="<?=$comment->getId()?>">

      <div>
        <label for="body">Комментарий</label>
        <input type="text" name="body" value="<?=$comment->getBody()?>">
      </div>

      <div>
        <label for="name">Автор</label>
        <input type="text" name="name" value="<?=$comment->getName()?>">
      </div>
      
      <div>
        <label for="">Блог</label>
        <select name="entry_id">
          <?php foreach (Entries::all($pdo) as $entry):?>
          <option value="<?=$entry->getId()?>" <?php if ($entry->getId() == $comment->getEntryId()) echo 'selected'?>><?=$entry->getTitle()?></option>
          <?php endforeach;?>
        </select>
      </div>

      <div>
        <input type="submit" name="submit" value="Редактировать">
      </div>

    </form>
  </div>

</body>
</html>
