<?php
require_once '../config/db.php';
require_once '../classes/Comments.php';

if (isset($_POST['submit']))
{
  foreach ($_POST as $key => $value)
  {
    $_POST[$key] = htmlspecialchars($value);
  }

  Comments::delete($_POST['id'], $pdo);

  header('location:../index.php');
}
