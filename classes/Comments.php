<?php


class Comments
{
  protected $id = 0;
  protected $name = '';
  protected $body = '';
  protected $entryId = 0;

  public function __construct($name, $body, $entryId)
  {
    $this->name = $name;
    $this->body = $body;
    $this->entryId = $entryId;
  }

  public function getId()
  {
    return $this->id;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getBody()
  {
    return $this->body;
  }

  public function getEntryId()
  {
    return $this->entryId;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function store(PDO $pdo)
  {
    try
    {
      $sql = '
            INSERT INTO comments SET 
            name = :name,
            body = :body,
            entry_id = :entry_id
           ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':name', $this->name);
      $sth->bindValue(':body', $this->body);
      $sth->bindValue(':entry_id', $this->entryId);
      $sth->execute();

      header('location:../index.php');
    }
    catch (Exception $exception)
    {
      echo 'Не удалось сохранить в БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }


  }

  static public function getById($id, PDO $pdo)
  {
    try
    {
      $id = intval($id);
      $sql = '
            SELECT * FROM comments
            WHERE id = :id
           ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':id', $id);
      $sth->execute();
      $commentArr = $sth->fetchAll();
      $commentArr = $commentArr[0];

      $commentObj = new self($commentArr['name'], $commentArr['body'], $commentArr['entry_id']);
      $commentObj->setId($commentArr['id']);

      return $commentObj;
    }
    catch (Exception $exception)
    {
      echo 'Не удалось получить данные из БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }
  }

  static public function delete($id, PDO $pdo)
  {
    $commentObj = self::getById($id, $pdo);
    $commentObj->destroy($pdo);
  }

  public function destroy(PDO $pdo)
  {
    try
    {
      $sql = '
              DELETE FROM comments
              WHERE id = :id
             ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':id', $this->id);
      $sth->execute();
    }
    catch (Exception $exception)
    {
      echo 'Не удалось удалить элемент из БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }
  }

  static public function edit($id, $name, $body, $entryId, PDO $pdo)
  {
    $commentObj = new self($name, $body, $entryId);
    $commentObj->setId($id);
    $commentObj->update($pdo);
  }

  public function update($pdo)
  {
    try
    {

      $sql ='
           UPDATE comments SET
           name = :name,
           body = :body,
           entry_id = :entry_id
           WHERE id = :id           
          ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':name', $this->name);
      $sth->bindValue(':body', $this->body);
      $sth->bindValue(':entry_id', $this->entryId);
      $sth->bindValue(':id', $this->id);
      $sth->execute();
    }
    catch (Exception $exception)
    {
      echo 'Не удалось обновить БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }



  }

}