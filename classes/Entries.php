<?php

class Entries
{
  protected $id = 0;
  protected $title = '';
  protected $intro = '';
  protected $content = '';

  public function __construct($title, $intro, $content)
  {
    $this->title = $title;
    $this->intro = $intro;
    $this->content = $content;
  }

  public function getId()
  {
    return $this->id;
  }

  public function getTitle()
  {
    return $this->title;
  }

  public function getIntro()
  {
    return $this->intro;
  }

  public function getContent()
  {
    return $this->content;
  }

  public function setId(int $id)
  {
    $this->id = $id;
  }

  static public function all(PDO $pdo)
  {
    try
    {
      $sql = '
            SELECT * FROM entries
           ';
      $sth = $pdo->query($sql);
      $entries = $sth->fetchAll();
      $entriesObj = [];

      foreach ($entries as $entry)
      {
        $entryObj = new self($entry['title'], $entry['intro'], $entry['content']);
        $entryObj->setId($entry['id']);

        $entriesObj[] = $entryObj;
      }

      return $entriesObj;
    }
    catch (Exception $exception)
    {
      echo 'Не удалось получить данные из БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }

  }

  public function store(PDO $pdo)
  {
    try
    {
      $sql = '
            INSERT INTO entries SET
            title = :title,
            intro = :intro,
            content = :content
           ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':title', $this->title);
      $sth->bindValue(':intro', $this->intro);
      $sth->bindValue(':content', $this->content);
      $sth->execute();

      header('location:../index.php');
    }

    catch (Exception $exception)
    {
      echo 'Не удалось сохранить в БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }


  }

  static public function getById($id, PDO $pdo)
  {
    try
    {
      $sql = 'SELECT * FROM entries
            WHERE id = :id
           ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':id', $id);
      $sth->execute();
      $entiesArr = $sth->fetchAll();
      $entiesArr = $entiesArr[0];
      $entryObj = new self($entiesArr['title'], $entiesArr['intro'], $entiesArr['content']);
      $entryObj->setId($entiesArr['id']);
      return $entryObj;
    }
    catch (Exception $exception)
    {
      echo 'Не удалось получить данные из БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }

  }

  static public function delete(int $id, PDO $pdo)
  {
    $entryById = self::getById($id, $pdo);
    $entryById->destroy($pdo);
  }

  public function destroy(PDO $pdo)
  {
    try
    {
      $sql = '
            DELETE FROM entries
            WHERE id = :id
           ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':id', $this->id);
      $sth->execute();

      header('location:../index.php');
    }
    catch (Exception $exception)
    {
      echo 'Не удалось удалить из БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }

  }

  static public function edit($id, $title, $intro, $content, PDO $pdo)
  {
    $entryObj = new self($title, $intro, $content);
    $entryObj->setId($id);
    $entryObj->update($pdo);
  }

  public function update(PDO $pdo)
  {
    try
    {
      $sql = '
            UPDATE entries SET
            title = :title,
            intro = :intro,
            content = :content
            WHERE id = :id
           ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':id', $this->id);
      $sth->bindValue(':title', $this->title);
      $sth->bindValue(':intro', $this->intro);
      $sth->bindValue(':content', $this->content);
      $sth->execute();

    }
    catch (Exception $exception)
    {
      echo 'Не удалось обновить БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }




  }

  public function getComments($pdo)
  {
    try
    {
      $sql = '
            SELECT * FROM comments
            WHERE entry_id = :entryId
           ';
      $sth = $pdo->prepare($sql);
      $sth->bindValue(':entryId', $this->id);
      $sth->execute();
      $commentsArr = $sth->fetchAll();

      $commentsObj = [];

      foreach ($commentsArr as $comment)
      {
        $commentObj = new Comments($comment['name'], $comment['body'], $this->id);
        $commentObj->setId($comment['id']);
        $commentsObj[] = $commentObj;
      }

      return $commentsObj;
    }
    catch (Exception $exception)
    {
      echo 'Не удалось получить данные из БД. Код ошибки: '.$exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
    }


  }

}