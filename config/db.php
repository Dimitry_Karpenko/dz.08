<?php

try
{
  $dbParams = require_once 'dbParams.php';

  $dsn = "mysql:host={$dbParams['host']};dbname={$dbParams['dbname']}";

  $pdo = new PDO($dsn, $dbParams['user'], $dbParams['pass']);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->exec('SET NAMES "utf8"');
}
catch (Exception $exception)
{
  echo 'Ошибка подключения к базе данных! Код ошибки: '. $exception->getCode().'. Сообщение об ошибке: '.$exception->getMessage();
}




